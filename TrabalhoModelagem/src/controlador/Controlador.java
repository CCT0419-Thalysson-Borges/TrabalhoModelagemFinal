package controlador;

import model.Pessoa;
import model.Produto;
import model.ItemCompra;
import model.Compra;

public class Controlador{
	private Pessoa clienteCorrente;
	private Pessoa pessoa = new Pessoa();
	private Produto produto = new Produto();
	
	public static Controlador controlador = null;
	
	private Controlador(){}
	
	
	public static Controlador getInstance() throws Exception{
		if(controlador == null) {
			controlador = new Controlador();
		}else {
			throw new Exception("Controlador j existe!!");
		}
		return controlador;
	}


	public Pessoa getPessoa() {
		return pessoa;
	}


	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}


	public Produto getProduto() {
		return produto;
	}


	public void setProduto(Produto produto) {
		this.produto = produto;
	}


	public Pessoa getClienteCorrente() {
		return clienteCorrente;
	}


	public void setClienteCorrente(Pessoa clienteCorrente) {
		this.clienteCorrente = clienteCorrente;
	}

	
}