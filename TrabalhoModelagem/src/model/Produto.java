package model;

public class Produto {
	
	// atributos da classe Produto
	
	private String descricao;
	private float preco;
	
	// metodo construtor com parametros
	
	public Produto(String descricao, float preco) {
		this.descricao = descricao;
		this.preco = preco;
	}
	
	// metodo construtor
	
	public Produto() {
		
	}
	
	// metodos 
	
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public float getPreco() {
		return preco;
	}
	public void setPreco(float preco) {
		this.preco = preco;
	}
	

}
