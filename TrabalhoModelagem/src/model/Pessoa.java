package model;

import java.util.ArrayList;

public class Pessoa {
	
	// atributos da classe Pessoa
	
	private String nome;
	private String sobrenome;
	private ArrayList <Compra> compras = new ArrayList<Compra>();
	
	// metodo construtor com parametro
	
	public Pessoa(String nome, String sobrenome) {
		this.nome = nome;
		this.sobrenome = sobrenome;
	}
	
	// metodo construtor 
	
	public Pessoa() {
		
	}
	
	// metodos
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}

	public ArrayList <Compra> getCompras() {
		return compras;
	}

	public void adicionarCompra(Compra compra) {
		compras.add(compra);
	}
	
	
}
