package model;

import java.util.ArrayList;

public class Compra {
	private String numero;
	private ArrayList <ItemCompra> itens = new ArrayList<ItemCompra>();

	
	public Compra(String numero, ArrayList<ItemCompra> itens) {
		this.itens = itens;
		this.numero = numero;
	}
	
	public Compra() {}
	
	
	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}


	public ArrayList <ItemCompra> getItens() {
		return itens;
	}


	public void adicionarItem(ItemCompra item) {
		itens.add(item);
	}
	
	

}
