package model;

public class ItemCompra {
	private int quantidade;
	private float valor;
	private Produto produto;
	
	public ItemCompra(int quantidade, Produto produto) {
		this.quantidade = quantidade;
		this.valor += produto.getPreco()*quantidade;
		this.produto = produto;
	}
	
	public ItemCompra() {}
	
	
	
	public int getQuantidade() {
		return quantidade;
	}
	
	
	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}


	public float getValor() {
		return valor;
	}


	public void setValor(float valor) {
		this.valor = valor;
	}


	public Produto getProduto() {
		return produto;
	}


	public void adicionaProduto(Produto produto) {
		this.produto = produto;
	}
	

}
